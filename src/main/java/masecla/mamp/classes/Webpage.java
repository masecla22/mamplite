package masecla.mamp.classes;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import masecla.mamp.session.Session;

public abstract class Webpage {

	public String internalHtml(Website wb, UserClient client, MAMPEngine engine, Request request) {
		// Reset the headers;
		headers = new LinkedHashMap<String, List<String>>();
		return generateHTML(wb, client, engine, request);
	}

	public abstract String generateHTML(Website wb, UserClient client, MAMPEngine engine, Request request);

	public Map<String, List<String>> headers = new LinkedHashMap<String, List<String>>();

	/**
	 * This function will add a header that has the same key. Useful for cookies
	 * usually. This will NOT overwrite any of the previous values;
	 * 
	 * @param key   - The header name
	 * @param value - The value of the header to be added.
	 */
	protected void addHeader(String key, String value) {
		if (!this.headers.containsKey(key))
			this.headers.put(key, new ArrayList<String>());
		List<String> values = this.headers.get(key);
		values.add(value);
		this.headers.put(key, values);
	}

	/**
	 * This will create a session or return one if it was provided with the request
	 * 
	 * @param request
	 * @return The session created or retrieved;
	 */
	protected Session createSession(Website wb, Request request) {
		String c = null;
		try {
			for (String s : request.getHeaders().get("Cookie").split("; "))
				if (s.split("\\=")[0].equals("MAMP_SSID"))
					c = s.split("\\=")[1];
		} catch (Exception e) {
		}
		if (c == null) {
			Session s = wb.createSession();
			addHeader("set-cookie", s.createHeader());
			return s;
		} else {
			Session s = wb.getSession(c);
			if(s==null)
			{
				//This means that the cookie sent was invalid
				//Or the session expired and was removed from existence.
				//OR the MAMP process was restarted and the sessions got wiped
				s = wb.createSession();
			}
			addHeader("set-cookie", s.createHeader());
			return s;
		}
	}

	/**
	 * This function will set the header and the values. Worth nothing that any
	 * header values stored earlier will be completely removed.
	 * 
	 * @param key   - The header name
	 * @param value - The header value
	 */
	protected void setHeader(String key, String value) {
		List<String> values = new ArrayList<String>();
		values.add(value);
		this.headers.put(key, values);
	}
}
