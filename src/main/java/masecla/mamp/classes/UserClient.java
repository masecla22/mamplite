package masecla.mamp.classes;

import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.Map;

import masecla.mamp.builders.ResponseBuilder;
import masecla.mamp.exceptions.InvalidUserClientException;

public class UserClient {
	private Socket sock;
	private Request req;
	private boolean valid = true;
	private String forcedIp = null;

	public String getIP() {
		if (forcedIp != null)
			return forcedIp;
		return sock.getInetAddress().toString().replace("/", "");
	}

	public int getPort() throws InvalidUserClientException {
		if (valid)
			return sock.getPort();
		else
			throw new InvalidUserClientException();
	}

	public UserClient(Socket socket) {
		sock = socket;
		try {
			req = new Request(socket.getInputStream());
			if (req.getHeaders().containsKey("X-Forwarded-For")) {
				this.forcedIp = req.getHeaders().get("X-Forwarded-For");
			}
		} catch (Exception e) {
			e.printStackTrace();
			valid = false;
		}
	}

	public boolean isValid() {
		return valid;
	}

	public Socket getSocket() {
		return sock;
	}

	public Request getRequest() {
		return req;
	}

	/**
	 * @author Matt
	 * @deprecated due to the fact that it's direct sending to the socket which can
	 *             mess with the responses.
	 * @param message Represents the message to send
	 * @throws IOException Thrown if sending the bytes fails.
	 */
	@Deprecated
	public void send(String message) throws IOException {
		sock.getOutputStream().write(message.getBytes());
	}

	/**
	 * @author Matt
	 * @param response Represents the response to send to the client.
	 * @return Returns wether the sending completed successfully.
	 *
	 */
	public boolean send(Response response, Map<String, List<String>> headers) {
		ResponseBuilder bld = new ResponseBuilder(response).setHeaders(headers);
		return bld.build().send(this);
	}

	public void close() throws IOException {
		sock.close();
	}
}
