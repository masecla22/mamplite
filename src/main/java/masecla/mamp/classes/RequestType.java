package masecla.mamp.classes;

public enum RequestType {
	GET("GET"), HEAD("HEAD"), POST("POST"), PUT("PUT"), DELETE("DELETE"), CONNECT("CONNECT"), OPTIONS("OPTIONS"), TRACE(
			"TRACE"),UNDEFINED("UNDEFINED");
	private String representation;

	private RequestType(String rep) {
		representation = rep;
	}
	
	@Override
	public String toString(){
		return representation;
	}

	public static RequestType fromString(String str)
	{
		for(RequestType c : RequestType.values())
			if(c.toString().equalsIgnoreCase(str))
				return c;
		return UNDEFINED;
	}
}
