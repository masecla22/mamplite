package masecla.mamp.classes;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import masecla.mamp.exceptions.InvalidRequestException;
import masecla.mamp.utils.URLParser;

public class Request {
	private Map<String, String> hsm = new HashMap<String, String>();
	private String connectionString;
	private Map<String, String> params = new HashMap<String, String>();

	public RequestType getRequestType() {
		return RequestType.fromString(connectionString.split(" ")[0]);
	}

	/**
	 * 
	 * 
	 * @return - The url that the client requested
	 * @throws InvalidRequestException - If somehow the request is invalid
	 */
	public String getURLRequested() throws InvalidRequestException {
		try {
			return connectionString.split(" ")[1];
		} catch (Exception e) {
			throw new InvalidRequestException();
		}
	}

	/**
	 * Constructor that parses the packet.
	 * 
	 * @param packet - The connection String.
	 * @throws InvalidRequestException
	 */
	public Request(String packet) throws InvalidRequestException {
		connectionString = packet.split("\n")[0]; // Find the first line "GET / HTTP/1.1"
		String[] headers = packet.split("\r\n\r\n")[0].split("\n"); // Find the headers
		for (int i = 1; i < headers.length; i++) {
			hsm.put(headers[i].split(":")[0], headers[i].split(": ")[1]); // Parse all the headers
		}
		// Parse parameters
		if (getRequestType().equals(RequestType.GET)) {
			params = new URLParser(getURLRequested()).getParameters(); // Check all the parameters
		} else if (getRequestType().equals(RequestType.POST)) {
			try {
				// If it's a post request grab the rest of the data and put it into parameters
				String body = packet.split("\r\n\r\n")[1];
				// Iterate the body
				for (String param : body.split("&")) {
					String res = "";
					String key = "";
					int eqs = 0;
					for (int i = 0; i < param.length(); i++) {
						if (param.charAt(i) == '=') {
							eqs++;
						}
						if (eqs == 0)
							key += param.charAt(i);
						else {
							if (param.charAt(i) == '=' && eqs == 1)
								res += param.charAt(i);
							else
								res += param.charAt(i);
						}
					}
					res = res.substring(1);
					params.put(key, res);
				}
			} catch (Exception e) {
				// Body is malformed
			}
		}
	}

	/**
	 * The constructor for a Request
	 * 
	 * @param str - The input stream of the Socket
	 * @throws InvalidRequestException - If the packet sent is invalid
	 * @throws IOException             - Something went wrong
	 */
	public Request(InputStream str) throws InvalidRequestException, IOException {
		this(convert(str));
	}

	/**
	 * Get the headers for this request
	 * 
	 * @return - The headers
	 */
	public Map<String, String> getHeaders() {
		return this.hsm;
	}

	/**
	 * Returns the HTTP version being used.
	 * 
	 * @return - The HTTP version being used.
	 * @throws InvalidRequestException
	 */
	public String getHTTPVersion() {
		return connectionString.split(" ")[2];
	}

	/**
	 * Returns the parameters.
	 * 
	 * @return The Parameters
	 */
	public Map<String, String> getParameters() {
		return params;
	}

	/**
	 * A function that convers the input stream to the connection packet string
	 * 
	 * @param inputStream - The socket input stream
	 * @return - Returns the request string required
	 * @throws IOException - If something goes wrong.
	 */
	private static String convert(InputStream inputStream) throws IOException {
		// Start with the empty variables
		String s = "";
		int c = 1, cont = 0;
		// Iterate each byte
		while ((c = inputStream.read()) != -1) {
			// Concatenate it
			byte[] b = { (byte) c };
			String r = new String(b);
			s += r;
			if (r.equals("\n") || r.equals("\r"))
				cont++;
			else
				cont = 0;
			if (cont == 4) // The end of the string
				break;
		}
		// Handle the post
		if (s.contains("POST")) {
			// Find the length
			String sr = s.split("Content-Length: ")[1].split("\n")[0];
			int len = Integer.parseInt(sr.substring(0, sr.length() - 1));
			for (int i = 0; i < len; i++) {
				// Read the bytes
				c = inputStream.read();
				byte[] b = { (byte) c };
				String r = new String(b);
				// Add the end
				s += r;
			}
		}
		return s;
	}

}
