package masecla.mamp.classes;

import java.util.HashMap;

public class MAMPEngine {

	public MAMPEngine() {
	}

	// Config options to be help within
	private HashMap<String, String> configOptions = new HashMap<String, String>();
	// The InternalDataManager class

	/**
	 * This is a simple way to wrap around the HashMap<String,String> that keeps the
	 * options. This also adds the constraint that if the option is not real, it
	 * won't pop an error, just an empty value
	 * 
	 * @param option - The option to get
	 * @return - the value of the options required.
	 */
	public String getOption(String option) {
		// Check for existence of key
		if (configOptions.containsKey(option))
			// Return regular value
			return configOptions.get(option);
		// Return empty value if not existing
		return "";
	}
}
