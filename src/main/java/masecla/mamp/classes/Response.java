package masecla.mamp.classes;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Response {
	private int status;
	private String msg;
	private String server;
	private byte[] body;
	private String contenttype;
	private String httpver;
	private Map<String, List<String>> headers;

	public Response(int status, String msg, String server, byte[] body, String contenttype, String httpver,
			Map<String, List<String>> headers) {
		super();
		this.status = status;
		this.msg = msg;
		this.server = server;
		this.body = body;
		this.contenttype = contenttype;
		this.httpver = httpver;
		this.headers = headers;
	}

	public int getStatus() {
		return status;
	}

	public String getMessage() {
		return msg;
	}

	public String getServer() {
		return server;
	}

	public byte[] getBody() {
		return body;
	}

	public String getContentType() {
		return contenttype;
	}

	public String getHTTPVersion() {
		return httpver;
	}

	public Map<String, List<String>> getHeaders() {
		return headers;
	}

	/**
	 * This function will add a header that has the same key. Useful for cookies
	 * usually. This will NOT overwrite any of the previous values;
	 * 
	 * @param key   - The header name
	 * @param value - The value of the header to be added.
	 */
	public void addHeader(String key, String value) {
		if (!this.headers.containsKey(key))
			this.headers.put(key, new ArrayList<String>());
		List<String> values = this.headers.get(key);
		values.add(value);
		this.headers.put(key, values);
	}

	/**
	 * This function will set the header and the values. Worth nothing that any
	 * header values stored earlier will be completely removed.
	 * 
	 * @param key   - The header name
	 * @param value - The header value
	 */
	public void setHeader(String key, String value) {
		List<String> values = new ArrayList<String>();
		values.add(value);
		this.headers.put(key, values);
	}

	/**
	 * @author Matt
	 * @param socket - Represents the socket to send the response to.
	 * @return Returns wether the sending completed successfully.
	 *
	 */
	public boolean send(Socket socket) {
		// Create the first line
		String header = "HTTP/" + httpver + " " + status + " " + msg;
		setHeader("Server", server); // Add the server header
		setHeader("Content-Type", contenttype + "; charset=UTF-8"); // Add the Content-Type header
		setHeader("Content-Length", body.length + ""); // Add the Content-Length header
		for (String c : headers.keySet()) {
			List<String> headerValues = headers.get(c);
			for (String hV : headerValues)
				header += "\r\n" + c + ": " + hV; // Add the headers to the top
		}
		header += "\r\n\r\n"; // End the request
		// Append the body
		try {
			socket.getOutputStream().write(header.getBytes());// Write the headers
			socket.getOutputStream().write(body); // Write the body
		} catch (IOException e) {
			return false; // Something went wrong
		}
		return true; // Everything went well
	}

	/**
	 * @author Matt
	 * @param client Represents the client to send the response to.
	 * @return Returns wether the sending completed successfully.
	 *
	 */
	public boolean send(UserClient client) {
		return send(client.getSocket());
	}

}