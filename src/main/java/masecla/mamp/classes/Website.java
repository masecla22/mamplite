package masecla.mamp.classes;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import masecla.mamp.builders.ResponseBuilder;
import masecla.mamp.session.Session;
import masecla.mamp.session.SessionsManager;

public class Website {
	/**
	 * 
	 * @return Returns the port of the connection on which this was opened
	 */
	public int getPort() {
		try {
			return Integer.parseInt(this.getMAMPEngine().getOption("default-port"));
		} catch (Exception e) {
			return 80;
		}
	}

	public int getAllowedThreads() {
		return 10;
	}

	/**
	 * 
	 * @return The home directory where you can find all the stuff you need
	 */
	public File getHomeDirectory() {
		return new File(".");
	}

	private Map<String, Webpage> pages = new HashMap<String, Webpage>();
	private SessionsManager sessManager;
	private Webpage notfound = null;
	private MAMPEngine eng;
	private Webpage forbidden = null;
	private boolean open = false;
	private ExecutorService tasksManager = Executors.newFixedThreadPool(getAllowedThreads());
	private ServerSocket ssock;

	public Map<String, Webpage> getSitemap() {
		return pages;
	}

	public Session createSession() {
		return sessManager.createSession();
	}

	public Session getSession(String id) {
		return sessManager.getSession(id);
	}

	public void map(String url, Webpage page) {
		pages.put(url, page);
	}

	public void setNotFoundPage(Webpage page) {
		notfound = page;
	}

	public void setForbiddenPage(Webpage page) {
		forbidden = page;
	}

	public Webpage getNotFoundPage() {
		return notfound;
	}

	public Webpage getForbiddenPage() {
		return forbidden;
	}

	public MAMPEngine getMAMPEngine() {
		return eng;
	}

	public Website() {
		this.sessManager = new SessionsManager();
	}

	public void open() throws IOException {
		eng = new MAMPEngine();
		this.ssock = new ServerSocket(getPort());
		final Website st = this;
		open = true;
		while (open) {
			Socket ss = ssock.accept();
			Runnable r = () -> {
				try {
					UserClient client = new UserClient(ss);
					if (client.isValid()) {
						Response resp;
						resp = new ResponseBuilder().build(st, client, eng);
						resp.send(client);
					}
				} catch (Exception exc) {
				}
			};
			this.tasksManager.submit(r);
		}
	}

	public void close() {
		this.tasksManager.shutdown();
		try {
			this.ssock.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.open = false;
	}
}
