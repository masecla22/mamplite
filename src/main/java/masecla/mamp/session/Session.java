package masecla.mamp.session;

import java.time.Instant;
import java.util.HashMap;
import java.util.UUID;

public class Session {
	private String id;
	private HashMap<String, Object> boundObjects = new HashMap<>();
	private Long timestamp;

	public Session() {
		id = UUID.randomUUID().toString().replace("-", "");
		timestamp = Instant.now().getEpochSecond();
	}

	public Object getBoundObject(String key) {
		return boundObjects.get(key);
	}

	public String createHeader() {
		return "MAMP_SSID=" + id + "; path=/; HttpOnly";
	}

	public HashMap<String, Object> getAllBinds() {
		return this.boundObjects;
	}

	public void bindValue(String key, Object obj) {
		boundObjects.put(key, obj);
	}

	public String getID() {
		return id;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	@Override
	public String toString() {
		return id;
	}
}
