package masecla.mamp.session;

import java.util.ArrayList;
import java.util.List;

public class SessionsManager {
	private List<Session> activeSessions = new ArrayList<Session>();

	public Session createSession() {
		Session s = new Session();
		this.activeSessions.add(s);
		return s;
	}

	public Session getSession(String id) {
		for (Session s : activeSessions)
			if (s.getID().equals(id))
				return s;
		return null;
	}

	public void updateSession(Session sess) {
		for (int i = 0; i < activeSessions.size(); i++)
			if (sess.getID().equals(activeSessions.get(i).getID()))
				activeSessions.set(i, sess);
	}
}
