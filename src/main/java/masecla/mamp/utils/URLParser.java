package masecla.mamp.utils;

import java.util.HashMap;

public class URLParser {
	// The URL that is meant to be parsed
	private String url;

	/**
	 * @author Matt
	 * @param URL - The URL to be parsed
	 */
	public URLParser(String URL) {
		// Remove the backspace from the beginning. /help -> help
		if (URL.startsWith("\\"))
			url = URL.substring(1, URL.length());
		else
			url = URL;
	}

	/**
	 * A function to return all the paramters from the URL
	 * 
	 * @return A HashMap containing all the paramaters from the URL
	 */
	public HashMap<String, String> getParameters() {
		// This means that there are no parameters.
		if (url.split("\\?").length == 1)
			return new HashMap<String, String>(); // Return empty map
		else {
			String parameters = url.split("\\?")[1]; // Get the parameters path of the URL
			HashMap<String, String> result = new HashMap<String, String>();
			// Iterate each parameter
			for (String cr : parameters.split("&")) {
				// Check for corrupt parameters
				try {
					String res = "";
					String key = "";
					int eqs = 0;
					for (int i = 0; i < cr.length(); i++) {
						if (cr.charAt(i) == '=') {
							eqs++;
						}
						if (eqs == 0)
							key += cr.charAt(i);
						else {
							if (cr.charAt(i) == '=' && eqs == 1)
								res += cr.charAt(i);
							else
								res += cr.charAt(i);
						}
					}
					res = res.substring(1);
					result.put(key, res);
				} catch (IndexOutOfBoundsException e) {
					e.printStackTrace();
					continue;
				}
			}
			return result;
		}
	}
}
