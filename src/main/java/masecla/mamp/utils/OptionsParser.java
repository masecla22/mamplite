package masecla.mamp.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class OptionsParser {
	//The file which will be parsed.
	private File tfile;

	/**
	 * @author Matt
	 * @param file - The file to parse.
	 */
	public OptionsParser(File file) {
		tfile = file;
	}

	/**
	 * A function that returns all the options from the file.
	 * @author Matt
	 * @return A map containing all the values inside the file.
	 */
	public HashMap<String, String> parse() {
		//The end result.
		HashMap<String, String> result = new HashMap<String, String>();
		try {
			//The stream for the file
			InputStream str = new FileInputStream(tfile);
			BufferedReader r = new BufferedReader(new InputStreamReader(str));
			String line = null;
			//Iterate each line
			while ((line = r.readLine()) != null) {
				//Ignore the lines starting with # since they are comments.
				if (line.startsWith("#"))
					continue;
				else {
					//Format is option=value.
					String opt = line.split("#")[0]; 
					//Remove trailing spaces.
					opt = opt.substring(0, opt.endsWith(" ") ? opt.length() - 1 : opt.length());
					String key = opt.split("=")[0], value = ""; //Find the key
					for (int i = 1; i < opt.split("=").length; i++)
						value += opt.split("=")[i] + "=";
					value = value.substring(0, value.length() - 1); //Trim last character
					//Add to result
					result.put(key, value);
				}
			}
			//Close the stream
			r.close();
			//Pass the result
			return result;
		} catch (FileNotFoundException e) {
			//File doesn't exist, return empty HashMap
			return new HashMap<String, String>();
		} catch (IOException e) {
			//Something is not quite right, return empty HashMap
			return new HashMap<String, String>();
		}

	}
}
