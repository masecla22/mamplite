package masecla.mamp.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import masecla.mamp.classes.Website;

public class HTMLLoader {
	private Website wb = null;

	public HTMLLoader(Website st) {
		wb = st;
	}

	public HTMLLoader() {
		wb = new Website() {
			@Override
			public File getHomeDirectory() {
				return new File(".");
			}
		};
	}

	public String loadHtml(File file) {
		String path = wb.getHomeDirectory().getAbsolutePath() + File.separator + file.getPath();
		path.replace(File.separator + File.separator, File.separator);
		file = new File(path);
		try {
			return new String(Files.readAllBytes(file.toPath()), StandardCharsets.UTF_8);
		} catch (IOException e) {
			return "<html><body>"
					+ "An internal exception has occured and a file went missing. Notify the system admin."
					+ "</body></html>";
		}
	}

	public String loadHtml(InputStream input) {
		try {
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int length;
			while ((length = input.read(buffer)) != -1) {
				result.write(buffer, 0, length);
			}
			return result.toString("UTF-8");
		} catch (IOException e) {
			return "<html><body>"
					+ "An internal exception has occured and a file went missing. Notify the system admin."
					+ "</body></html>";
		}
	}
}
